#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

"Programa creado por Alberto García Zafra"

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    f_string = first.lower()
    s_string = second.lower()

    if f_string < s_string:
        return True
    else:
        return False


def sort_pivot(words: list, pos: int):
    """Sorts word in pivot position, moving it to the left until its place"""
    current_pos: int = pos
    while (current_pos > 0) and is_lower(words[current_pos], words[current_pos - 1]):
        # Intercambia los números
        words[current_pos], words[current_pos - 1] = words[current_pos - 1], words[current_pos]
        current_pos = current_pos - 1
    return words


def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    listaordenada = []
    for pivot_pos in range(1, len(words)):
        left = sort_pivot(words, pivot_pos)
        listaordenada = left
    return listaordenada


def show(words: list):
    """Show words on screen, using print()"""
    word = " ".join(words)
    showed = print(word)
    return showed


def main():
    words: list = sys.argv[1:]
    ordered:list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()
